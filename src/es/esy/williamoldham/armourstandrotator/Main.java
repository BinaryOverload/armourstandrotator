package es.esy.williamoldham.armourstandrotator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArmorStand;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	private HashMap<Player, Integer> editTemp = new HashMap<Player, Integer>();
	
	YamlConfiguration armourstandConfig;
	File armourstands;
	
	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		
		editTemp.clear();
		
		if(this.getDataFolder().exists()){
			armourstands = new File(this.getDataFolder(), "DONOTCHANGE.yml");
			
			if(!(armourstands.exists())){
				try {
					armourstands.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			armourstandConfig = YamlConfiguration.loadConfiguration(armourstands);
		} else {
			this.getDataFolder().mkdir();
			
			armourstands = new File(this.getDataFolder(), "DONOTCHANGE.yml");
			
			if(!(armourstands.exists())){
				try {
					armourstands.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			armourstandConfig = YamlConfiguration.loadConfiguration(armourstands);
		}
		
		Set<String> uuids = armourstandConfig.getConfigurationSection("uuids").getKeys(false);
		getLogger().info(uuids.toString());
		
		for(World world : Bukkit.getServer().getWorlds()){
			for(Entity entity : world.getEntities()){
				
				String entityUUID = entity.getUniqueId().toString();
				@SuppressWarnings("unused")
				boolean isArmourStand = entity instanceof ArmorStand;
				
				getLogger().info(entity.toString());
				
				for(String uuid : uuids){
					getLogger().info(entityUUID);
					if(entityUUID == uuid.trim()){
						
						getLogger().info("HEY!");
						
						Integer speed = armourstandConfig.getInt("uuids." + uuid);

						CraftArmorStand cas = (CraftArmorStand) entity;
						ArmorStand as = cas;

						Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

							@Override
							public void run() {

								if(as != null){
									Location l = as.getLocation();
									if(l.getYaw() + 18 > 360){
										l.setYaw(0 + 18);
										as.teleport(l);
									} else {
										l.setYaw(l.getYaw() + 18);
										as.teleport(l);
									}
								}

							}
						}, 0, speed);
					}
				}
			}
		}
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(command.getName().equalsIgnoreCase("rotate")) {
			if(!(sender instanceof Player)){
				sender.sendMessage(ChatColor.RED + "Only players can do this command!");
				return true;
			}
			if(args.length == 0){
				Player player = (Player) sender;
				
				player.sendMessage(ChatColor.RED + "Wrong command format!");
				player.sendMessage(ChatColor.RED + "/rotate <speed>");

				return true;
			} else if (args.length >= 2){
				Player player = (Player) sender;
				
				player.sendMessage(ChatColor.RED + "Wrong command format!");
				player.sendMessage(ChatColor.RED + "/rotate <speed>");

				return true;
			} else {
				Player player = (Player) sender;
				
				if(parseInt(args[0], player) != null){
					Integer speed = parseInt(args[0], player);

					editTemp.put(player, speed);
					return true;
				}
				return false;
			}
		}
		
		return false;
		
	}
	
	private Integer parseInt(String num, Player player){
		try {
			return Integer.parseInt(num);
		} catch (NumberFormatException e) {
			player.sendMessage(ChatColor.RED + "Error parsing number! Make sure you are enetering a valid number!");
		}
		return null;
	}
	
	@EventHandler
	public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent e){
		if(editTemp.containsKey(e.getPlayer()) && e.getRightClicked() instanceof ArmorStand){
			e.getPlayer().sendMessage("Test");
			ArmorStand as = (ArmorStand) e.getRightClicked();
			
			Integer speed = editTemp.get(e.getPlayer());
			
			Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
				
				@Override
				public void run() {
					
					if(as != null){
						Location l = as.getLocation();
						if(l.getYaw() + 18 > 360){
							l.setYaw(0 + 18);
							as.teleport(l);
						} else {
							l.setYaw(l.getYaw() + 18);
							as.teleport(l);
						}
					}
					
				}
			}, 0, speed);
			
			armourstandConfig.set("uuids." + as.getUniqueId().toString(), speed);
			try {
				armourstandConfig.save(armourstands);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			editTemp.remove(e.getPlayer());
		}
	}

	
}
